module top
(
	input logic clock,//Pin 23
	input logic reset,//Pin 25
	
	//Vga connections
	output logic hsync,//Pin 101
	output logic vsync,//Pin 103
	output logic r,//Pin 106
	output logic g,//Pin 105
	output logic b//Pin 104
	
	//TODO buttons
);
softvga_vga_if vgaIF();
assign hsync = vgaIF.hsync;
assign vsync = vgaIF.vsync;
assign r = vgaIF.r;
assign g = vgaIF.g;
assign b = vgaIF.b;

logic clock25;

always_ff @(posedge clock)
begin
	clock25 <= !clock25;
end


softvga_top (.*, .clock(clock25));

endmodule : top