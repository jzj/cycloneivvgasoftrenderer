interface softvga_vga_if
();

logic hsync;
logic vsync;
logic r, g, b;

modport out (output hsync, vsync, r, g, b);

endinterface : softvga_vga_if