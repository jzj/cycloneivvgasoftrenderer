/* Displayed resolution is 640x480@60hz. Framebuffer is 160x120. */
interface softvga_vgafb_if
();

logic [10:0] address;
logic [29:0] readData;
logic writeEnable;
logic [29:0] writeData;

//Used by risc v core
modport core (input readData, output address, writeEnable, writeData);

//Used by vga controller
modport controller (input address, writeEnable, writeData, output readData);

endinterface : softvga_vgafb_if

module softvga_vga//VGA controller
(
	input logic clock,//Should be 25.175 mhz
	input logic reset,
	
	//VGA interface
	softvga_vga_if.out vgaIF,
	
	//Framebuffer interface for risc v core
	softvga_vgafb_if.controller fbIF
);
//VGA parameters (https://web.mit.edu/6.111/www/s2004/NEWKIT/vga.shtml)
localparam HPIXEL_ACTIVE = 640;
localparam HPIXEL_FP = 16;
localparam HPIXEL_HSYNC = 96;
localparam HPIXEL_BP = 48;
localparam VLINES_ACTIVE = 480;
localparam VLINES_FP = 11;
localparam VLINES_VSYNC = 2;
localparam VLINES_BP = 31;

//Thresholds for output modes (all inclusive)
localparam HPIXEL_ACTIVE_BEGIN = 0;
localparam HPIXEL_FP_BEGIN = HPIXEL_ACTIVE;
localparam HPIXEL_HSYNC_BEGIN = HPIXEL_ACTIVE + HPIXEL_FP;
localparam HPIXEL_BP_BEGIN = HPIXEL_ACTIVE + HPIXEL_FP + HPIXEL_HSYNC;
localparam HPIXEL_MAX = HPIXEL_ACTIVE + HPIXEL_FP + HPIXEL_HSYNC + HPIXEL_BP - 1;
localparam VLINES_ACTIVE_BEGIN = 0;
localparam VLINES_FP_BEGIN = VLINES_ACTIVE;
localparam VLINES_HSYNC_BEGIN = VLINES_ACTIVE + VLINES_FP;
localparam VLINES_BP_BEGIN = VLINES_ACTIVE + VLINES_FP + VLINES_VSYNC;
localparam VLINES_MAX = VLINES_ACTIVE + VLINES_FP + VLINES_VSYNC + VLINES_BP - 1;

//Framebuffer control
logic readEnable;
logic [10:0] address;
logic [29:0] fbData;

//Pixel logic
logic [9:0] hpixel;
logic [8:0] vlines;
logic [9:0] hpixel_next;
logic [8:0] vlines_next;

//Pixel incrementing logic
always_comb
begin
	if (hpixel_next <= HPIXEL_MAX)
		hpixel_next = hpixel + 10'd1;
	else
		hpixel_next = 10'd0;
	
	if (vlines_next <= VLINES_MAX)
		vlines_next = vlines + 9'd1;
	else
		vlines_next = 9'd0;
end

always_ff @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		hpixel <= 10'd0;
		vlines <= 9'd0;
	end
	else if (clock)
	begin
		hpixel <= hpixel_next;
		vlines <= vlines_next;
	end
end

//Sync pulse logic
always_ff @(posedge clock, posedge reset)
begin
	if (reset)
	begin
		vgaIF.hsync <= 1'b1;
		vgaIF.vsync <= 1'b1;
	end
	else if (clock)
	begin
		if (hpixel_next == HPIXEL_HSYNC_BEGIN)
			vgaIF.hsync <= 1'b0;
		else if (hpixel_next == HPIXEL_BP_BEGIN)
			vgaIF.hsync <= 1'b1;
			
		if (vlines_next == VLINES_HSYNC_BEGIN)
			vgaIF.vsync <= 1'b0;
		else if (vlines_next == VLINES_BP_BEGIN)
			vgaIF.vsync <= 1'b1;
	end
end

//Colour output/framebuffer fetch logic

softvga_vgafb vgaFB (.*);

endmodule : softvga_vga

//Modules for internal use

module softvga_vgafb
(
	input logic clock,
	
	//Lines for vga controller
	input logic readEnable,
	input logic [10:0] address,
	output logic [29:0] fbData,//Read 10 pixels at a time
	
	//Framebuffer interface for risc v core
	softvga_vgafb_if.controller fbIF
);

endmodule